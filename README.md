# bc\_dkucc\_jupyter\_singularity

## Name
bc\_dkucc\_jupyter\_singularity

## Description
[Open OnDemand](https://openondemand.org/) Batch Connect application that will be used to launch Juyper Lab from inside various Singularity/Apptainer images. We will have a library of images that will have various packages. The user can also choose to run a Singularity/Apptainer image they created that contains Jupyter Lab and whatever packages their application needs.
